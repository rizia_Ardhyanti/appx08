package ardhyanti.rizia.appx08

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.ContextMenu
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_setting.*

class MainActivity : AppCompatActivity() {
    val RC_SETTING_SUKSES : Int = 100

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.option_menu,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId) {
            R.id.itemSetting -> {
                var intent = Intent(this, SettingActivity::class.java)
                //intent.putExtra("X",tvDetail.text.toString())
                startActivityForResult(intent,RC_SETTING_SUKSES)
                return true
            }
            else -> {
                Toast.makeText(this, "Tidak ada yang dipilih", Toast.LENGTH_SHORT).show()
                return false
            }
        }
        return super.onOptionsItemSelected(item)
    }

}
